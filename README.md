# Converting MS-COCO annotations to Pascal VOC format 

[//]: # (Image References)
[image1]: ./images/xxx.png


![alt text][image1]  

## Why this?  
for some reason ... adding unknown classes ... continual learning ...

this SSD implementation: https://github.com/pierluigiferrari/ssd_keras/blob/master/ssd300_training.ipynb

modified to use the optimizer developed by Zenke et al., (https://github.com/ganguli-lab/pathint) which implements their "consolidation" method for continual learning.


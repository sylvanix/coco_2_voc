'''

'''

import os.path
import pickle
import csv
from xml.dom.minidom import parseString
from lxml.etree import Element, SubElement, tostring


task_A = ['fire hydrant', 'stop sign', 'parking meter', 'kite', 'surfboard']
task_B = ['banana', 'apple', 'orange', 'broccoli', 'hot dog']

WRITE_OUT = True

imgs_dir = '/home/davros/testing/CML/ssd_keras/datasets/ms-coco_2014/train2014'
infile = 'annotations_2014_dict.pkl'
annotations_dict = pickle.load(open(infile, 'rb'))
out_dir = './test2014_xml'
out_dir_txt = './test2014_txt'

A_test_rows = []
A_count = 0
for img_id in annotations_dict:
    img_path = imgs_dir + '/' + annotations_dict[img_id]['file_name']
    out_path = out_dir + '/' + annotations_dict[img_id]['file_name'].split('.jpg')[0] + '.xml'
    try:
        os.path.getsize(img_path)
        print( annotations_dict[img_id]["file_name"],
               annotations_dict[img_id]["height"],
               annotations_dict[img_id]["width"],
               annotations_dict[img_id]["bbox"],
               annotations_dict[img_id]["category_id"],
               annotations_dict[img_id]["name"] )    
        
        # SAVE INFO FOR THIS IMAGE TO VOC-STYLE XML FILE
        height = annotations_dict[img_id]["height"]
        width = annotations_dict[img_id]["width"]
        channels = 3
        class_id = annotations_dict[img_id]["category_id"]
        nb_instances = len( annotations_dict[img_id]["name"] )
        bb = annotations_dict[img_id]["bbox"]
        #print("bb: ", bb)
        
        node_root = Element('annotation')
        node_folder = SubElement(node_root, 'folder')
        node_folder.text = 'coco2017'
        img_name = annotations_dict[img_id]["file_name"]
        
        node_filename = SubElement(node_root, 'filename')
        node_filename.text = img_name
        
        node_source= SubElement(node_root, 'source')
        node_database = SubElement(node_source, 'database')
        node_database.text = 'Coco database'
        
        node_size = SubElement(node_root, 'size')
        node_width = SubElement(node_size, 'width')
        node_width.text = str(width)
    
        node_height = SubElement(node_size, 'height')
        node_height.text = str(height)

        node_depth = SubElement(node_size, 'depth')
        node_depth.text = str(channels)

        node_segmented = SubElement(node_root, 'segmented')
        node_segmented.text = '0'
        
        # are there any overlapping classes (mixture of classes from tasks A & B)?
        found_A = sum([1 for x in annotations_dict[img_id]["name"] if x in task_A])
        found_B = sum([1 for x in annotations_dict[img_id]["name"] if x in task_B])
        
        newLabelsList = []
        task_A_count = 0
        if found_B == 0:
            for nb in range(nb_instances):
                isTaskA = False
                if annotations_dict[img_id]["name"][nb] in task_A:
                    isTaskA = True
                    task_A_count += 1
                    
                    
                if isTaskA:
                    # ms-coco order is [x,y,width,height]  # the bounding boxes are in this order
                    # voc order is [class_id, xmin, xmax, ymin, ymax]
                    new_label = [ annotations_dict[img_id]["file_name"].split('.')[0] ]
                    A_test_rows.append(new_label)
                    A_count += 1
             
                    # ms-coco order is [x,y,width,height]  # the bounding boxes are in this order
                    # voc order is [class_id, xmin, xmax, ymin, ymax]
                    new_label = [ class_id[nb],
                                  int(bb[nb][0]),   # xmin
                                  int(bb[nb][0] + (bb[nb][2])),     # xmax ### good
                                  int(bb[nb][1]),   # ymin
                                  int(bb[nb][1] + (bb[nb][3])) ]    # ymax ### good                          
                    
                    if new_label[1] < 0: new_label[1] = 0
                    if new_label[2] > width-1:  new_label[2] = width-1             
                    if new_label[3] < 0: new_label[3] = 0
                    if new_label[4] > height-1:  new_label[4] = height-1   
                    newLabelsList.append(new_label)                          

                    node_object = SubElement(node_root, 'object')
                    node_name = SubElement(node_object, 'name')
                    node_name.text = annotations_dict[img_id]["name"][nb]                      
                    
                    node_pose = SubElement(node_object, 'pose')
                    node_pose.text = 'Unspecified'
                    
                    node_truncated = SubElement(node_object, 'truncated')
                    node_truncated.text = '0'
                    node_difficult = SubElement(node_object, 'difficult')
                    node_difficult.text = '0'
                    node_bndbox = SubElement(node_object, 'bndbox')
                    node_xmin = SubElement(node_bndbox, 'xmin')
                    node_xmin.text = str(new_label[1])
                    node_ymin = SubElement(node_bndbox, 'ymin')
                    node_ymin.text = str(new_label[3])
                    node_xmax = SubElement(node_bndbox, 'xmax')
                    node_xmax.text =  str(new_label[2])
                    node_ymax = SubElement(node_bndbox, 'ymax')
                    node_ymax.text = str(new_label[4])
                    xml = tostring(node_root, pretty_print=True)  
                    dom = parseString(xml)     
            
            if WRITE_OUT:
                if task_A_count > 0:
                    f = open(out_path, "wb")
                    f.write(xml)
                    f.close()             
        

        
    except IOError:
         print("IMAGE NOT FOUND: " + img_path)
         #break
        
print(len(A_test_rows))
if (WRITE_OUT and A_test_rows):
    with open(out_dir_txt + '/' + 'taskA_test.txt', 'w', newline='') as f1:
        writer = csv.writer(f1)
        writer.writerows(A_test_rows)
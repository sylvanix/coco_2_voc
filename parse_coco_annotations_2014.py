'''
Reads the ms-coco annotations JSON file and saves the result to a picled dictionary
which has the following structure:
    annot_dict = { image_id:{'bbox':[], 'category_id':[], 'name':[]
                             'file_name':'', 'height':int, 'width':int} }
Note that whereas an image has only one entry for file_name, height, and width,
it can have multiple for bbox, category_id, and name, hence these are written
to lists.                             
'''

import json
import pickle


annot_path = '/home/davros/testing/CML/ssd_keras/datasets/ms-coco_2014/annotations'
imgs_path  = '/home/davros/testing/CML/ssd_keras/datasets/ms-coco_2014/train2014'

annot_file = annot_path + '/instances_train2014.json'

categories = [ {'supercategory': 'person', 'id': 1, 'name': 'person'},
               {'supercategory': 'vehicle', 'id': 2, 'name': 'bicycle'},
               {'supercategory': 'vehicle', 'id': 3, 'name': 'car'},
               {'supercategory': 'vehicle', 'id': 4, 'name': 'motorcycle'},
               {'supercategory': 'vehicle', 'id': 5, 'name': 'airplane'},
               {'supercategory': 'vehicle', 'id': 6, 'name': 'bus'},
               {'supercategory': 'vehicle', 'id': 7, 'name': 'train'},
               {'supercategory': 'vehicle', 'id': 8, 'name': 'truck'},
               {'supercategory': 'vehicle', 'id': 9, 'name': 'boat'},
               {'supercategory': 'outdoor', 'id': 10, 'name': 'traffic light'},
               {'supercategory': 'outdoor', 'id': 11, 'name': 'fire hydrant'},
               {'supercategory': 'outdoor', 'id': 13, 'name': 'stop sign'},
               {'supercategory': 'outdoor', 'id': 14, 'name': 'parking meter'},
               {'supercategory': 'outdoor', 'id': 15, 'name': 'bench'},
               {'supercategory': 'animal', 'id': 16, 'name': 'bird'},
               {'supercategory': 'animal', 'id': 17, 'name': 'cat'},
               {'supercategory': 'animal', 'id': 18, 'name': 'dog'},
               {'supercategory': 'animal', 'id': 19, 'name': 'horse'},
               {'supercategory': 'animal', 'id': 20, 'name': 'sheep'},
               {'supercategory': 'animal', 'id': 21, 'name': 'cow'},
               {'supercategory': 'animal', 'id': 22, 'name': 'elephant'},
               {'supercategory': 'animal', 'id': 23, 'name': 'bear'},
               {'supercategory': 'animal', 'id': 24, 'name': 'zebra'},
               {'supercategory': 'animal', 'id': 25, 'name': 'giraffe'},
               {'supercategory': 'accessory', 'id': 27, 'name': 'backpack'},
               {'supercategory': 'accessory', 'id': 28, 'name': 'umbrella'},
               {'supercategory': 'accessory', 'id': 31, 'name': 'handbag'},
               {'supercategory': 'accessory', 'id': 32, 'name': 'tie'},
               {'supercategory': 'accessory', 'id': 33, 'name': 'suitcase'},
               {'supercategory': 'sports', 'id': 34, 'name': 'frisbee'},
               {'supercategory': 'sports', 'id': 35, 'name': 'skis'},
               {'supercategory': 'sports', 'id': 36, 'name': 'snowboard'},
               {'supercategory': 'sports', 'id': 37, 'name': 'sports ball'},
               {'supercategory': 'sports', 'id': 38, 'name': 'kite'},
               {'supercategory': 'sports', 'id': 39, 'name': 'baseball bat'},
               {'supercategory': 'sports', 'id': 40, 'name': 'baseball glove'},
               {'supercategory': 'sports', 'id': 41, 'name': 'skateboard'},
               {'supercategory': 'sports', 'id': 42, 'name': 'surfboard'},
               {'supercategory': 'sports', 'id': 43, 'name': 'tennis racket'},
               {'supercategory': 'kitchen', 'id': 44, 'name': 'bottle'},
               {'supercategory': 'kitchen', 'id': 46, 'name': 'wine glass'},
               {'supercategory': 'kitchen', 'id': 47, 'name': 'cup'},
               {'supercategory': 'kitchen', 'id': 48, 'name': 'fork'},
               {'supercategory': 'kitchen', 'id': 49, 'name': 'knife'},
               {'supercategory': 'kitchen', 'id': 50, 'name': 'spoon'},
               {'supercategory': 'kitchen', 'id': 51, 'name': 'bowl'},
               {'supercategory': 'food', 'id': 52, 'name': 'banana'},
               {'supercategory': 'food', 'id': 53, 'name': 'apple'},
               {'supercategory': 'food', 'id': 54, 'name': 'sandwich'},
               {'supercategory': 'food', 'id': 55, 'name': 'orange'},
               {'supercategory': 'food', 'id': 56, 'name': 'broccoli'},
               {'supercategory': 'food', 'id': 57, 'name': 'carrot'},
               {'supercategory': 'food', 'id': 58, 'name': 'hot dog'},
               {'supercategory': 'food', 'id': 59, 'name': 'pizza'},
               {'supercategory': 'food', 'id': 60, 'name': 'donut'},
               {'supercategory': 'food', 'id': 61, 'name': 'cake'},
               {'supercategory': 'furniture', 'id': 62, 'name': 'chair'},
               {'supercategory': 'furniture', 'id': 63, 'name': 'couch'},
               {'supercategory': 'furniture', 'id': 64, 'name': 'potted plant'},
               {'supercategory': 'furniture', 'id': 65, 'name': 'bed'},
               {'supercategory': 'furniture', 'id': 67, 'name': 'dining table'},
               {'supercategory': 'furniture', 'id': 70, 'name': 'toilet'},
               {'supercategory': 'electronic', 'id': 72, 'name': 'tv'},
               {'supercategory': 'electronic', 'id': 73, 'name': 'laptop'},
               {'supercategory': 'electronic', 'id': 74, 'name': 'mouse'},
               {'supercategory': 'electronic', 'id': 75, 'name': 'remote'},
               {'supercategory': 'electronic', 'id': 76, 'name': 'keyboard'},
               {'supercategory': 'electronic', 'id': 77, 'name': 'cell phone'},
               {'supercategory': 'appliance', 'id': 78, 'name': 'microwave'},
               {'supercategory': 'appliance', 'id': 79, 'name': 'oven'},
               {'supercategory': 'appliance', 'id': 80, 'name': 'toaster'},
               {'supercategory': 'appliance', 'id': 81, 'name': 'sink'},
               {'supercategory': 'appliance', 'id': 82, 'name': 'refrigerator'},
               {'supercategory': 'indoor', 'id': 84, 'name': 'book'},
               {'supercategory': 'indoor', 'id': 85, 'name': 'clock'},
               {'supercategory': 'indoor', 'id': 86, 'name': 'vase'},
               {'supercategory': 'indoor', 'id': 87, 'name': 'scissors'},
               {'supercategory': 'indoor', 'id': 88, 'name': 'teddy bear'},
               {'supercategory': 'indoor', 'id': 89, 'name': 'hair drier'},
               {'supercategory': 'indoor', 'id': 90, 'name': 'toothbrush'} ]

id2nameDict = {}
for x in categories:
    id2nameDict[x['id']] = x['name']
#print(id2nameDict)

with open(annot_file, 'r') as fh:
    annotDict = json.load(fh)
    
print([x for x in annotDict])

# annotDict has the keys:
# info        : <class 'dict'>
#    where info has the keys: ['description', 'url', 'version', 'year', 'contributor', 'date_created']
# licenses    : <class 'list'>
# images      : <class 'list'>
# annotations : <class 'list'>
# categories  : <class 'list'>

keys = []
for k in annotDict: # (info, licenses, images, annotations, categories) 
    keys.append(k)

# determine the number of images referenced by this annotations file
for k in keys:
    if k == 'annotations':
        nb_annots = len(annotDict[k])
print(nb_annots)

# create a dictionary with "id" (the image file name) as root key
annotInfoDict = {}
for k in keys:
    if k == "annotations":
        for nb in range(nb_annots):
            print([x for x in annotDict[k][nb]]) #['segmentation', 'area', 'iscrowd', 'image_id', 'bbox', 'category_id', 'id']
            thisId = annotDict[k][nb]["image_id"]
            if thisId not in annotInfoDict:
                annotInfoDict[thisId] = {}
                annotInfoDict[thisId]["bbox"] = []
                annotInfoDict[thisId]["category_id"] = []
                annotInfoDict[thisId]["name"] = []
            else:
                annotInfoDict[thisId]["bbox"].append(annotDict[k][nb]["bbox"])
                annotInfoDict[thisId]["category_id"].append(annotDict[k][nb]["category_id"])
                annotInfoDict[thisId]["name"].append(id2nameDict[ annotDict[k][nb]["category_id"] ])

# for id in annotInfoDict:
#     print(annotInfoDict[id])

# determine the number of images referenced by this images file
for k in keys:
    if k == 'images':
        nb_imgs = len(annotDict[k])
print(nb_imgs)        

for k in keys:
    if k == "images":
        for nb in range(nb_imgs):            
            #cd print( [x for x in annotDict[k][nb]] ) # ['license', 'file_name', 'coco_url', 'height', 'width', 'date_captured', 'flickr_url', 'id']
            thisId = annotDict[k][nb]['id']
            if thisId in annotInfoDict:
                annotInfoDict[thisId]["file_name"] = annotDict[k][nb]["file_name"]
                annotInfoDict[thisId]["height"] = annotDict[k][nb]["height"]
                annotInfoDict[thisId]["width"] = annotDict[k][nb]["width"]

                # print( annotInfoDict[thisId]["file_name"],
                #        annotInfoDict[thisId]["height"],
                #        annotInfoDict[thisId]["width"],
                #        annotInfoDict[thisId]["bbox"],
                #        annotInfoDict[thisId]["category_id"],
                #        annotInfoDict[thisId]["name"] )                


outfile = 'annotations_2014_dict.pkl'
pickle.dump(annotInfoDict, open(outfile, 'wb'))
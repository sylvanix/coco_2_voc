import matplotlib.pyplot as plt
import numpy as np
import warnings

warnings.filterwarnings("ignore", category=UserWarning)
# https://matplotlib.org/examples/color/named_colors.html
colors = {1:'deepskyblue', 2:'lightskyblue', 3:'limegreen', 4:'orangered',
          5:'red', 6:'gold', 7:'snow'}

fc='black'
cm='gray'
theme_col = colors[3]
fig, ax = plt.subplots(1, 1)#, figsize=(7,7))
fig.patch.set_facecolor(fc)
fig.set_figwidth(10)
fig.set_figheight(8)


def show_img_return_input(img, name, cm='gray', ask=True):
    plt.ion()
    plt.imshow(img, cmap=cm)
    plt.show()
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    plt.tight_layout()
    plt.draw()
    if ask:
        accept = input('OK? ')
    else:
        accept = 'y'
        plt.pause(0.0001)
    plt.cla()
    return(accept)


def write_img(img, name, out_dir, cm='gray'):
    theme_col = "red"
    plt.ion()
    #plt.imshow(img, cmap=cm, vmin=vmin, vmax=vmax)
    plt.imshow(img, cmap=cm)
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    plt.xticks([])
    plt.yticks([])
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    plt.savefig(out_dir+'/'+name, bbox_inches='tight')
    plt.cla()
    
    
############################################################################################    
        
'''
WITH KEYBOARD INTERRUPT


USAGE:

DISPLAYING = True
count = 0
while (DISPLAYING):
    if count == 0:
        initial_cid = custom_plots.show_img_kb(img, 'test')
    else
        cid = custom_plots.show_img_kb(img, 'test')
        if cid != initial_cid:
            DISPLAYING = False
    count += 1

'''

'''
 onkeypress()
 a function taking input from the keyboard to change settings or quit the program
'''
cid_dict = {}
clip_dict = {'inc':0, 'dec':0}
def onkeypress(event):

    if event.key in ['', 'q']:
        print('Terminating program')
        plt.gcf().canvas.mpl_disconnect(cid_dict['cid'])
        del cid_dict['cid']
        
    if event.key == 'i':
        print('Raising Upper clip level ... (showing only brighter objects)')
        clip_dict['inc'] = 1
        
    if event.key == 'I':
        print('Raising Upper clip level ... (showing only brighter objects)')
        clip_dict['inc'] = 5        
        
    if event.key == 'd':
        print('Lowering Upper clip level ... (to see dimmer objects)')
        clip_dict['dec'] = 1
        
    if event.key == 'D':
        print('Lowering Upper clip level ... (to see dimmer objects)')
        clip_dict['dec'] = 5            
        
    elif event.key == 'shift':
        pass
    else:
        print('Press "q" to quit, "i" to increment, or "d" to decrement.')


def show_img_kb(img, name, cm='gray'):
    plt.ion()
    plt.imshow(img, cmap=cm)
    plt.show()
    cid_dict['cid'] = plt.gcf().canvas.mpl_connect('key_press_event', onkeypress)
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    plt.tight_layout()
    plt.draw()
    plt.pause(0.0001)
    plt.cla()
    ax.cla()
    if cid_dict:
        return(cid_dict, clip_dict)
    else:
        return(-1)
